from django.test import TestCase
from django.urls import resolve
from .views import *


def test_url_exist(self):
    response = self.client.get("/")
    self.assertEqual(response.status_code,200)

def test_function_index(self):

    found = resolve("/")
    self.assertEqual(found.func, home)

def test_page_contains_npm(self):
    page_response = self.client.get("/")
    self.assertContains(page_response, "NPM: 1806241210")
# Create your tests here.
