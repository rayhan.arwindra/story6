from django.shortcuts import render,redirect
from .forms import statusForm
from .models import Status
# Create your views here.
def home(request):
    if request.method == "POST":
        form = statusForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = statusForm()
    status = Status.objects.all()
    return render(request,'pages/index.html',{'form':form,'statuses': status})