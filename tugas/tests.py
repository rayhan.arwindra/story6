from django.test import TestCase
from .models import Status
from django.urls import resolve
from .views import *
from .forms import statusForm

class unitTests(TestCase):
    
    def test_url_exist(self):

        response = self.client.get("/")
        self.assertEqual(response.status_code,200)

    def test_template(self):
        response = self.client.get("/")
        self.assertTemplateUsed(response,'pages/index.html')
    
    def test_function_index(self):

        found = resolve("/")
        self.assertEqual(found.func, home)

    def test_page_contains_hello(self):

        page_response = self.client.get("/")
        self.assertContains(page_response, "Hello, how are you?")

    def test_status_create_new(self):

        new_model = Status.objects.create(status = "Hello there")
        model_count =  Status.objects.all().count()

        self.assertEqual(model_count,1)

    def test_status_attribute(self):
        status_message = "Hello there"
        new_model = Status.objects.create(status = status_message)

        self.assertEqual(new_model.status, status_message)

    def test_status_str(self):
        new_model = Status.objects.create(status = "Hello there")
        self.assertEqual(str(new_model), new_model.status)

    def test_valid_post(self):
        form = statusForm({'status':'hi'})
        self.assertTrue(form.is_valid())


    def test_post_form_new_status(self):
        response = self.client.post('', {'status':'hi'}, follow = True)
        html_response =response.content.decode('utf8')
        self.assertIn('hi', html_response)
    
    def test_post_not_valid(self):
        
        form = statusForm({'status':''})
        self.assertFalse(form.is_valid())

    
# Create your tests here.
